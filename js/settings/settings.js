var globalSettingHandler = function() {
  /*
   * SOUND
   */
  var backgroundSoundVolume = 0.25;
  var monsterVolume = 1;
  var maxSoundDistMonster = 50;

  var getBackgroundSoundVolume = function() {
    return backgroundSoundVolume;
  };

  var setBackgroundSoundVolume = function(newVal) {
    backgroundSoundVolume = newVal;
  };

  var getMonsterVolume = function() {
    return monsterVolume;
  };

  var setMonsterVolume = function(newVal) {
    monsterVolume = newVal;
  };

  var getMaxMonsterSoundDist = function() {
    return maxSoundDistMonster;
  };

  var setMaxMonsterSoundDist = function(newVal) {
    maxSoundDistMonster = newVal;
  };

  /*
   * Interactivity
   */
  var maxRange = 5;
  var interactiveMeshNames = [
    //["Name", "ShortName (used in Blender !)", "Description", "Command to execute when player interacts with it"]
    ["TurningPoint", "TP", "Point where the monster is able to turn", ''],
    ["Altar", "Al", "Altars has to be activated to escape", 'interActionWithAltar(mesh)'],
    ["Exit", "Ex", "Interact with it to win the game", 'console.log("YOU ESCAPED")'], //No Exit yet
    ["Closet", "Cl", "Hide in it to not get seen... in theory", 'interactionWithCloset(mesh)']
  ];

  var getMaxInteractionRange = function() {
    return maxRange;
  };

  var getInteractiveMeshNames = function() {
    return interactiveMeshNames;
  };

  var addInteractiveMeshNames = function(name) {
    interactiveMeshNames.push(name);
  };

  var resetInteractiveMeshNames = function() {
    interactiveMeshNames = [];
  };

  /*
   *  Altars, Exits and Closets
   */

  var altarsToUnlockExit = 3;

  var getAltarUnlockNumber = function() {
    return altarsToUnlockExit;
  };

  var getNumberOfUnlockedAltars = function() {
    var Altars = map.getAltarArray();
    var unlocked = 0;
    Altars.forEach(function(mesh) {
      if (mesh._isActive) unlocked += 1;
    });
    return unlocked;
  };

  return {
    //SOUND
    getBackgroundSoundVolume: getBackgroundSoundVolume,
    setBackgroundSoundVolume: setBackgroundSoundVolume,
    getMonsterVolume: getMonsterVolume,
    setMonsterVolume: setMonsterVolume,
    getMaxMonsterSoundDist: getMaxMonsterSoundDist,
    setMaxMonsterSoundDist: setMaxMonsterSoundDist,
    //Interactivity
    getMaxInteractionRange: getMaxInteractionRange,
    getInteractiveMeshNames: getInteractiveMeshNames,
    addInteractiveMeshNames: addInteractiveMeshNames,
    resetInteractiveMeshNames: resetInteractiveMeshNames,
    //Altars, Exits and Closets
    getAltarUnlockNumber: getAltarUnlockNumber,
    getNumberOfUnlockedAltars: getNumberOfUnlockedAltars
  };
}();
