//Get Keycode: http://keycode.info/

var controls = (function() {
  var keys = {
    //Movement
    forward: 87, //W
    left: 65, //A
    right: 68, //D
    back: 83, //S
    //Interactivity
    interact: 69 //E
  };
  var speed = 1;
  var maxSpeed = speed + 0.5; //For people trying to cheat....

  var setCamSpeed = function(newSpeed) {
    if (newSpeed > maxSpeed) return;
    if (newSpeed < 0) return;
    camera.speed = newSpeed;
  };

  var getCamSpeed = function() {
    return speed;
  };

  var getKeys = function() {
    return keys;
  };

  var change = function(direction, num) {
    if (keys.hasOwnProperty(direction)) {
      keys[direction] = num;
    }
    load();
  };

  var load = function() {
    camera.inputs.remove(camera.inputs.attached.mouse);
    camera.keysUp.push(keys.forward);
    camera.keysLeft.push(keys.left);
    camera.keysDown.push(keys.back);
    camera.keysRight.push(keys.right);
    setCamSpeed(speed);
  };

  var keyDownHandler = function(keyCode) {
    if (keyCode == (keys.forward || keys.back || keys.left || keys.right)) return; //Movement is handled by BABYLONJS !
    if (keyCode == keys.interact) interactHandler.inquiry();
  };

  return {
    getKeys: getKeys,
    load: load,
    change: change,
    setCamSpeed: setCamSpeed,
    getCamSpeed: getCamSpeed,
    keyDownHandler: keyDownHandler
  };
})();;

var pressedKey = [];
document.addEventListener("keydown", function(e) { //Key-Listener
  pressedKey[e.keyCode] = true;
  controls.keyDownHandler(e.keyCode);
}, false);
document.addEventListener("keyup", function(e) {
  pressedKey[e.keyCode] = false;
}, false);
