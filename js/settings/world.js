var worldSettings = (function() {

  var changeGravity = function(newVal) {
    scene.gravity = new BABYLON.Vector3(0, newVal, 0);
  };

  var getGravity = function() {
    return scene.gravity;
  };

  return {
    changeGravity: changeGravity,
    getGravity: getGravity
  };
})();
