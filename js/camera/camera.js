var cameraHandling = (function() {

  var init = function() {
    document.addEventListener('mousemove', function(event) {
      if (pointerlock.currentState() == false) return;
      if (document.pointerLockElement === canvas || document.mozPointerLockElement === canvas) {
        var movementY = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
        var movementX = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

        camera.rotation.y += movementY * 0.002;
        camera.rotation.x = Math.max(-Math.PI / 3, Math.min(Math.PI / 3, camera.rotation.x + (movementX * 0.002)));
      }
    }, false);
  };

  var collision = function() {
    camera.applyGravity = true;
    camera.ellipsoid = new BABYLON.Vector3(1, 1, 1);
    scene.collisionsEnabled = true;
    camera.checkCollisions = true;
  };

  var pickResult = function() {
    var windowWidth = engine.getRenderWidth();
    var windowHeight = engine.getRenderHeight();
    var f = engine.getHardwareScalingLevel();
    var pick = scene.pick(windowWidth / 2 * f, windowHeight / 2 * f, null, false, camera);
    return pick;
  };

  return {
    init: init,
    collision: collision,
    pickResult: pickResult
  };
})();
