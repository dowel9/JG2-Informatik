var interactHandler = function() {

  var inquiry = function() { //inquiry = Anfrage
    var pick = cameraHandling.pickResult();
    if (!pick.hit) return; //You didn't hit a mesh
    var distance = pick.distance;
    var maxDistance = globalSettingHandler.getMaxInteractionRange();

    //if (distance < maxDistance) return; //Distance too high !
    if (!pick.pickedMesh._isInteractive) return; //Can not interact with this mesh !

    interact(pick.pickedMesh);
  };

  var interact = function(mesh) {
    var command = null;
    var allowedMeshesToInteract = globalSettingHandler.getInteractiveMeshNames();
    for (var i = 0, len = allowedMeshesToInteract.length; i < len; i++) {
      if (allowedMeshesToInteract[i].includes(mesh.name.substr(0, 2))) command = allowedMeshesToInteract[i][3];
    }

    eval(command);
  };

  var interactionWithCloset = function(closet) {
    if (!closet._isActive) {
      closet._isActive = true;
      controls.setCamSpeed(0); //You cannot move in closet
      camera.position = closet.position; //Move inside of closet
    } else {
      closet._isActive = false;
      controls.setCamSpeed(controls.getCamSpeed());
      camera.position.y += 10; //NOT GOOD ! NEED POINT IN FRONT OF CLOSET WHEN EXITING
    }
  };

  var interActionWithAltar = function(altar) {
    if (altar._isActive) return; //Altar is active
    altar._isActive = true;
    var activeAltars = globalSettingHandler.getNumberOfUnlockedAltars();
    var needAltars = globalSettingHandler.getAltarUnlockNumber();
    if (activeAltars >= needAltars) console.log("UNLOCKED EXIT");
  };

  return {
    inquiry: inquiry
  };
}();
