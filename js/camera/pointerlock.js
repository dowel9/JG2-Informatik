var pointerlock = (function() {
  var isLocked = false;
  //var canvas = document.getElementById("renderCanvas");
  var element = canvas;

  var init = function() {
    //console.log("Initializiere Pointer-Lock!")
    if ("onpointerlockchange" in document) {
      event = document.addEventListener('pointerlockchange', change, false);
    } else if ("onmozpointerlockchange" in document) {
      event = document.addEventListener('mozpointerlockchange', change, false);
    }
  };

  var change = function() {
    if (document.pointerLockElement === element || document.mozPointerLockElement === element) {
      isLocked = true;
      enter();
    } else {
      isLocked = false;
      exit();
    }
  };

  var exit = function() {
    document.exitPointerLock();
  };

  var enter = function() {
    canvas.requestPointerLock = canvas.requestPointerLock || canvas.msRequestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
    if (canvas.requestPointerLock) canvas.requestPointerLock();
  };

  var currentState = function() {
    return isLocked;
  };

  init();

  return {
    init: init,
    change: change,
    exit: exit,
    enter: enter,
    currentState: currentState
  };
})();

document.addEventListener('click', pointerlock.enter, false);
