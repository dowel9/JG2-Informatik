/*
 * All music/effects from:
 *  - http://soundimage.org/horrorsurreal/
 *  - https://www.freesoundeffects.com/free-sounds/
 *  - http://www.freesound.org
 *  - or they are homemade !
 * Supported formats: .wav .mp3 .ogg
 */

var monsterSoundHandling = function() {

  var init = function() {
    var monsterFootstepSound = new BABYLON.Sound("backgroundMusikGame", "sounds/footsteps.mp3", scene, null, {
      loop: true,
      autoplay: true,
      volume: globalSettingHandler.getMonsterVolume(),
      maxDistance: globalSettingHandler.getMaxMonsterSoundDist()
    });
    var backgroundMusikGame = new BABYLON.Sound("backgroundMusikGame", "sounds/After-the-Invasion.mp3", scene, null, {
      loop: true,
      autoplay: true,
      volume: globalSettingHandler.getBackgroundSoundVolume()
    });
    monsterFootstepSound.attachToMesh(monster);
  };

  var playRandomSentence = function() {
    var sentences = [
      ["sounds/kalinka.mp3"] //Don't play it :)
    ];
    var random = tools.randomizer(0, sentences.length);
    var sound = new BABYLON.Sound("backgroundMusikGame", sentences[random][0], scene, null, {
      loop: false,
      autoplay: true,
      volume: globalSettingHandler.getMonsterVolume(),
      maxDistance: globalSettingHandler.getMaxMonsterSoundDist()
    });
  };

  return {
    init: init,
    playRandomSentence: playRandomSentence
  };
}();
