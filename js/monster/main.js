//Raycasting: http://babylonjs-playground.com/#1QF77H#1
//Raycasting: http://www.babylonjs-playground.com/#1NE4XO#13

var monster;
var monsterIsBlocked = false;
var currentTarget;

var monsterHandling = (function() {
  var lastTPChange;
  var speed = new BABYLON.Vector3(0.1, 0, 0.1);
  var movementVecMonster = new BABYLON.Vector3(0, 0, 0);
  var overrideWay = false;
  var givenWay;
  //var maxAngle = 30; //Maximum Degree the Monster can detect you

  var init = function() {
    BABYLON.SceneLoader.ImportMesh('', 'objects/', 'monster.babylon', scene, function(meshes) {
      console.log("Loading Monster !");
      monster = meshes[0];
      ray.loadDestinations();
      checkTP();
      setInterval(checkTP, 256);
      setInterval(move, 16);
      //monsterSoundHandling.init();
    });
  };

  var move = function() {
    if (monsterIsBlocked) {
      //Don't move pls !
    } else {
      // console.log(monster.position, movementVecMonster);
      monster.position.addInPlace(movementVecMonster);
    }
  };

  var checkTP = function() {
    TP.forEach(function(mesh) {
      var distance = BABYLON.Vector3.Distance(monster.position, mesh.position);
      if (distance < 3.513 && lastTPChange != mesh) { //Monster-Y ist at ~2.513
        // console.log("TP ! W/ DISTANCE:" + distance);
        lastTPChange = mesh;
        var otherPos = turn(mesh);
        movementVecMonster = otherPos.position.clone().subtract(monster.position).normalize().multiply(speed); //Get Vector between mesh+Monster then normalize it and then get it on speed level
      }
    });
  };

  var turn = function(mesh) {
    var max = mesh._destinations.length;
    randomNum = tools.randomizer(1, max + 1) - 1;
    currentTarget = mesh._destinations[randomNum];
    tools.lookAtPoint(monster, currentTarget.position);
    return mesh._destinations[randomNum];
  };

  var getTarget = function() {
    return currentTarget;
  };

  return {
    init: init,
    move: move,
    checkTP: checkTP,
    turn: turn,
    getTarget,
  }
})();
