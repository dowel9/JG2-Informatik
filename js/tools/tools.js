var tools = (function() {

  var getDistance = function(vec1, vec2) {
    //vec1 and vec2 are vectors !
    var nk = 2; //Nachkommastellen
    var dist = Math.floor((100 * BABYLONS.Vector3.Distance(vec1, vec2))) / 100;
    return dist;
  };

  var randomizer = function(min, max) {
    var rand = Math.floor(Math.random() * (max - min) + min);
    return rand;
  };

  var getVector = function(vec1, vec2) {
    //Vector (result) from point1 (vec1) to point2 (vec2)
    var result = vec2.subtract(vec1);
    return result;
  };

  var getEinheitsVector = function(vector) {
    var dist = vector.normalize();
    return vector;
  };

  var lookAtPoint = function(mesh, point) {
    /*
     * mesh = mesh to rotate.
     * point = vector3(xyz) of target position to look at
     * THANKS TO: http://www.babylonjs-playground.com/#Q4LKP#2
     */
    point = point.subtract(mesh.position);
    mesh.rotation.y = -Math.atan2(point.z, point.x); // - Math.PI/2;
  };

  var getDistOfVector = function(vector3) {
    dist = Math.sqrt(Math.pow(vector3.x, 2), Math.pow(vector3.y, 2), Math.pow(vector3.z, 2));
    return dist;
  };

  return {
    getDistance: getDistance,
    randomizer: randomizer,
    getVector: getVector,
    getEinheitsVector: getEinheitsVector,
    getDistOfVector: getDistOfVector,
    lookAtPoint: lookAtPoint
  };

})();;
