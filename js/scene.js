var sceneHandling = (function() {
  canvas = document.getElementById("renderCanvas");
  engine = new BABYLON.Engine(canvas, true);

  var createScene = function() {
    // create a basic BJS Scene object
    scene = new BABYLON.Scene(engine);
    var light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), scene);

    //Get new Camera and apply Gravity and Collision for it
    camera = new BABYLON.FreeCamera('playerCam', new BABYLON.Vector3(0, 5, -10), scene);
    //camera = new BABYLON.ArcRotateCamera('playerCam', 50, 0.8, 200, new BABYLON.Vector3(0, 0, 0), scene);
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);
    controls.load();
    cameraHandling.init();
    cameraHandling.collision();

    //Loading Map
    map.load(scene);
    //Laoding Monster
    monsterHandling.init();

    return scene;
  };

  return {
    createScene: createScene
  };
})();
