var engine;
var scene;
var canvas;
var camera;

var main = function() {
  scene = sceneHandling.createScene();
  worldSettings.changeGravity(-9.81);
  cameraHandling.init()
  cameraHandling.collision();

  engine.runRenderLoop(function() {
    scene.render();
  });

  window.addEventListener('resize', function() {
    engine.resize();
  });
};

window.onload = main;
