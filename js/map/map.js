var TP = [];

var map = (function() {
  var AL = []; //Altar Array

  var load = function(scene) {
    console.log("Loading map...");
    BABYLON.SceneLoader.ImportMesh('', 'objects/', 'map.babylon', scene, function(newMeshes) {
      newMeshes[0].position = new BABYLON.Vector3(0, 0, 0);

      var totalTP = -1;
      scene.meshes.forEach(function(mesh) {
        var start = mesh.name.substr(0, 2);
        if (start == "TP") {
          totalTP++;
          //mesh.isVisible = false;
          mesh.checkCollisions = false;
          TP[totalTP] = mesh;
        } else {
          mesh.checkCollisions = true;
          mesh._isInteractive = false;
          if (start == "Al" || start == "Ex" || start == "Cl") { //Altar or Exit as interactive Meshes
            mesh._isInteractive = true;
            mesh._isActive = false; //When active: You are in closet or you've activated an Altar or unlocked the exit
          }
          if (start == "Al") {
            AL.push(mesh);
          };
        }
        for (var length = TP.length, i = 0; i < length; i++) {
          TP[i].position.y = 1;
        };
      });
    });
  };

  var getCurrent = function() {
    console.log("Not available yet !");
  };

  var getAltarArray = function() {
    return AL;
  };

  return {
    load: load,
    getCurrent: getCurrent,
    getAltarArray: getAltarArray
  };
})();
