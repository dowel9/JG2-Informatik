const ray = (function() {
  function loadDestinations() {
    scene.getMeshByName('Cube').isVisible = false;
    TP.forEach(function(mesh) {
      mesh.isVisible = false;
      mesh._destinations = [];
      var startPoint = mesh.position.clone();
      camera.position = startPoint;
      TP.forEach(function(otherMesh) {
        if (otherMesh != mesh) {
          var endPoint = otherMesh.position.clone();
          camera.lockedTarget = endPoint;
          var pickedResult = cameraHandling.pickResult();
          if (pickedResult.pickedMesh.name == otherMesh.name) {
            mesh._destinations.push(otherMesh);
            var distance = BABYLON.Vector3.Distance(mesh.position, otherMesh.position);
            mesh._destinations[mesh._destinations.length - 1]._distance = distance;
          }
        }
      });
      mesh.isVisible = true;
    });
    camera.lockedTarget = null;
    camera.position.y = 2.5;
    scene.getMeshByName('Cube').isVisible = true;
  }

  return {
    loadDestinations,
  };
})();
